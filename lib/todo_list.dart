import 'package:flutter/material.dart';
import 'package:todo_task/widget/widget.dart';

class TodoList extends StatefulWidget {
  const TodoList({super.key});

  @override
  _TodoListState createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {
  final TextEditingController _textFieldController = TextEditingController();
  final List<Todo> _todos = <Todo>[];
  final ValueNotifier<bool> _surfixShow = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Todo list'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        children: _todos.map((Todo todo) {
          return TodoItem(
            todo: todo,
            onTodoChanged: checklistUnCheckList,
          );
        }).toList(),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () => _displayDialog(),
          tooltip: 'Add Item',
          child: const Icon(Icons.add)),
    );
  }

  void checklistUnCheckList(Todo todo) {
    setState(() {
      todo.checklist = !todo.checklist;
    });
  }

  void _addTodoItem(String notes) {
    setState(() {
      _todos.add(Todo(notes: notes, checklist: false));
    });
    _textFieldController.clear();
  }

  Future<void> _displayDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Add a new todo item'),
          content: _textFieldRequiredMethod(
              title: "Type your new todo",
              textInputType: TextInputType.text,
              controller: _textFieldController),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
            ),
            TextButton(
              child: const Text('Add'),
              onPressed: () {
                setState(() {
                  if (_textFieldController.text.isNotEmpty) {
                    _addTodoItem(_textFieldController.text);
                    Navigator.pop(context);
                  }

                  Navigator.pop(context);
                });
              },
            ),
          ],
        );
      },
    );
  }

  TextFormField _textFieldRequiredMethod({
    required String title,
    required TextInputType textInputType,
    required TextEditingController controller,
    double? setTextFontSize,
  }) {
    return TextFormField(
      controller: controller,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      maxLines: 1,
      autofocus: false,
      validator: (value) {
        if (value != null) {
          return null;
        } else {
          return 'Enter a valid email';
        }
      },
      textAlign: TextAlign.left,
      onFieldSubmitted: (String submit) {},
      keyboardType: textInputType,
    );
  }
}
