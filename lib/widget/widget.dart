import 'package:flutter/material.dart';

class Todo {
  Todo({required this.notes, required this.checklist});
  bool checklist;
  final String notes;
}

class TodoItem extends StatelessWidget {
  TodoItem({
    required this.todo,
    required this.onTodoChanged,
  }) : super(key: ObjectKey(todo));

  final Todo todo;
  final onTodoChanged;

  TextStyle? _getTextStyle(bool checklist) {
    if (!checklist) return null;

    return const TextStyle(
      color: Colors.black54,
      decoration: TextDecoration.lineThrough,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        onTodoChanged(todo);
      },
      leading: CircleAvatar(
        child: Text(todo.notes[0]),
      ),
      title: Text(todo.notes, style: _getTextStyle(todo.checklist)),
    );
  }
}
