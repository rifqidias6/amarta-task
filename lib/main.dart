import 'package:flutter/material.dart';
import 'package:todo_task/todo_list.dart';

void main() => runApp(const TodoApp());
class TodoApp extends StatelessWidget {
  const TodoApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Todo list',
      home: TodoList(),
    );
  }
}
